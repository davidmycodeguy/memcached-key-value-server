# KeyValServer

[![N|Solid](https://i.imgur.com/sNvfAsj.png)](https://i.imgur.com/sNvfAsj)

KeyValServer is a key-value server, which mimics Memcached protocol loosely.
It supports following commands

1.  GET
    Syntax :-
    `GET <KEY>`
    If key is present it'll return:- <Key>: Val
    If Key is not present it'll return: <Key> not present

2.  SET
    Syntax :-
    `SET <KEY> <VALUE> [<ttl>]`
    Here ttl is optional and is not honored in current implementation.
    If Success, it'll return a SUCCESS message
    If failed, it'll return An internal error have occurred message.

3.  DELETE

    Syntax:-
    `DELETE <KEY>`
    If Success, it'll return a SUCCESS message
    If failed, it'll return An internal error have occurred message.

# Running server

-   open application root directory and let's call it $BASEDIR
    CD $BASEDIR/src

-   src directory contains bootstrap.sh file. Run this file using the following command:-
    ./bootstrap.sh

-   in shell type `telnet localhost 11211`

-   Then set a few example key-value pairs

-   open $BASEDIR/src/index.html to see key-value pairs with a React UI.

### Todos / Scope for improvement

Following things can be improved in the current app.

-   Implement very good logging
-   Implement user-friendly error messages in the server.
-   Implement hooks for handling shutdown gracefully in the server.
-   Use SQLAlchemy OR other ORM for DB relation operations.
-   Use connection pool(as well as other databases) to support serving multiple concurrent requests.

## License

MIT

**Free Software, Hell Yeah!**
