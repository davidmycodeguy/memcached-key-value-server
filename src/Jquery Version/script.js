(
function(){
    function createTable(responseText)
    {
        var jsonResponse = JSON.parse(responseText);
        var str = "";
        jQuery.each(jsonResponse, function (key, value) {
            str += '<tr><td>' + (key + 1) + '</td>'
                + '<td>' + value[0] + '</td>'
                + '<td>' + value[1] + '</td>'
                + '<td>' + value[2] + '</td></tr>';
        });
        jQuery('table[id=key-val-table] tbody').html(str);
    }


    function getKeyValSvrStats()
    {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == XMLHttpRequest.DONE)
            {
                createTable(xhr.responseText);
            }
        }

        xhr.open('GET', '/api/key-server-stats', true);
        xhr.send(null);
    }

    getKeyValSvrStats();

    setInterval(getKeyValSvrStats, 5000);
})();
