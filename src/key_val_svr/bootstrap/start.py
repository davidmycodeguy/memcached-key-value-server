import sys

from key_val_svr.components.console.datastore import InMemoryDataStore
from key_val_svr.components.console.datastore.sqlite import SqliteDataStore
from key_val_svr.components.console.server import KeyValServer
from key_val_svr.components.console.server_handler import KeyValServerRequestHandler
from key_val_svr.components.http.server import KeyValueWebServer, KeyValueHttpRequestHandler
from key_val_svr.settings import DATA_STORE_TYPE, IN_MEMORY_DATASTORE, KEY_SERVER_CONSOLE_PORT, KEY_SERVER_CONSOLE_HOST, \
    SQLITE_DATASTORE, KEY_SERVER_WEB_CONSOLE_HOST, KEY_SERVER_WEB_CONSOLE_PORT


def get_data_store(**kwargs):
    if DATA_STORE_TYPE == IN_MEMORY_DATASTORE:
        return InMemoryDataStore()

    elif DATA_STORE_TYPE == SQLITE_DATASTORE:
        return SqliteDataStore(**kwargs)

    assert False, "Shouldn't reach here."


if __name__ == "__main__":
    file_name = sys.argv[1]
    data_store = get_data_store(file_name=file_name)
    server = KeyValServer((KEY_SERVER_CONSOLE_HOST, KEY_SERVER_CONSOLE_PORT),
                          KeyValServerRequestHandler, data_store=data_store)

    server.start()
    print ("Started KeyValue Server.")

    web_server = KeyValueWebServer((KEY_SERVER_WEB_CONSOLE_HOST, KEY_SERVER_WEB_CONSOLE_PORT),
                                   KeyValueHttpRequestHandler, data_store=data_store)
    web_server.start()
    print ("Started Web Server.")
