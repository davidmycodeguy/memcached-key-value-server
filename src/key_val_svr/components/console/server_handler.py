import socketserver

from key_val_svr.components.console.exceptions import InvalidCommandException
from key_val_svr.components.console import commands


class KeyValServerRequestHandler(socketserver.BaseRequestHandler):

    @classmethod
    def cmd_cls(cls, cmd):
        if cmd.lower() not in ["get", "set", "delete"]:
            raise InvalidCommandException([cmd])

        cmd_cls_name = "{}{}Command".format(cmd[0].upper(), cmd[1:].lower())
        cmd_cls = getattr(commands, cmd_cls_name)

        assert cmd_cls, "cmdCls: {} not found".format(cmd_cls_name)
        return cmd_cls

    def handle(self):
        stream_content = self.request.recv(1024)
        data = str(stream_content, encoding='utf-8')
        inputs = data.split(" ")

        response_msg = b""
        if len(inputs) < 2:
            response_msg = bytes("Invalid command: {}".format(data), encoding='utf-8')

        else:
            inputs = [input.strip() for input in inputs]
            command = inputs[0]

            try:
                cmd_cls = self.cmd_cls(command)

            except AssertionError:
                response_msg = bytes("Invalid command: {}".format(command), encoding='utf-8')

            else:
                try:
                    cmd_cls_instance = cmd_cls(inputs[1:])

                except Exception as e:
                    print (e)
                    response_msg = bytes("Internal error have occured", encoding='utf-8')

                else:
                    out = self.server.invoke_cmd(cmd_cls_instance)
                    response_msg = bytes(str(cmd_cls_instance.get_msg(out)), encoding='utf-8')

        self.request.sendall(response_msg)