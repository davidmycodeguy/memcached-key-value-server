class DataStoreBase(object):

    def set(self, cmd):
        raise NotImplementedError()

    def delete(self, cmd):
        raise NotImplementedError()

    def get(self, cmd):
        raise NotImplementedError()

    def get_stats(self):
        raise NotImplementedError()
