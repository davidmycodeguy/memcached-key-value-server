from copy import deepcopy

from key_val_svr.components.console.datastore.base import DataStoreBase


class InMemoryDataStore(DataStoreBase):
    def __init__(self):
        self.values = {}

    def set(self, cmd):
        self.values[cmd.key] = (cmd.val, cmd.ttl)

    def delete(self, cmd):
        try:
            out = self.values.pop(cmd.key)

        except KeyError:
            return None

        return out

    def get(self, cmd):
        out = self.values.get(cmd.key)

        return out[0] if out else None

    def get_stats(self):
        return [(key, ) + value for key, value in self.values.items()]
