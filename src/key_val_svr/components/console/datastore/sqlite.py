import sqlite3

from key_val_svr.components.console.datastore.base import DataStoreBase
from key_val_svr.settings import SQLITE_TABLE_NAME


class SqliteDataStore(DataStoreBase):

    def __init__(self, **kwargs):
        self.file_name = kwargs.get("file_name")
        self._con = None

    def _create_table(self):
        cursor = self.connection.cursor()
        CREATE_TABLE_QUERY = """CREATE TABLE IF NOT EXISTS {}(
            KEY_NAME TEXT not null primary key,
            KEY_VALUE TEXT not null,
            KEY_TTL INTEGER
        );
        """.format(SQLITE_TABLE_NAME)

        out = cursor.execute(CREATE_TABLE_QUERY)
        self.connection.commit()

        return out

    @property
    def connection(self):
        if not self._con:
            self._con = sqlite3.connect(self.file_name)
            self._create_table()

        return self._con

    def set(self, cmd):
        query = """INSERT OR REPLACE INTO {}
        (KEY_NAME, KEY_VALUE, KEY_TTL)
        VALUES
        (?, ?, ?)
        """.format(SQLITE_TABLE_NAME)

        cursor = self.connection.cursor()
        out = cursor.execute(query, [cmd.key, cmd.val, cmd.ttl])
        self.connection.commit()

        return out

    def get_no_of_records_affected(self):
        query = "SELECT changes() from {}".format(SQLITE_TABLE_NAME)

        cursor = self.connection.cursor()
        cursor.execute(query)

        out = cursor.fetchone()

        if out:
            return out[0]

        return 0

    def delete(self, cmd):
        query = "DELETE FROM {} WHERE KEY_NAME=?".format(SQLITE_TABLE_NAME)

        cursor = self.connection.cursor()
        cursor.execute(query, [cmd.key])

        self.connection.commit()

        rows_changed = self.get_no_of_records_affected()
        return rows_changed == 1

    def get(self, cmd):
        query = "SELECT KEY_VALUE FROM {} WHERE KEY_NAME=?".format(SQLITE_TABLE_NAME)

        cursor = self.connection.cursor()
        out = cursor.execute(query, [cmd.key])

        record = out.fetchone()
        if record:
            return record[0]

        return record

    def get_stats(self):

        query = "SELECT * FROM {}".format(SQLITE_TABLE_NAME)

        connection = sqlite3.connect(self.file_name)
        cursor = connection.cursor()
        out = cursor.execute(query)

        return out.fetchall()
