class InvalidCommandException(Exception):
    def __init__(self, cmd):
        return super(InvalidCommandException, self).__init__(" ".join(cmd))


class InvalidCommandOutputException(Exception):
    def __init__(self, out):
        return super(InvalidCommandOutputException, self).__init__(str(out))
