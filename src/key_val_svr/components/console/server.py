import socketserver
from threading import Thread


class KeyValServer(socketserver.TCPServer):
    def __init__(self, *args, **kwargs):
        data_store = kwargs.pop("data_store", None)
        self.data_store = data_store

        super(KeyValServer, self).__init__(*args, **kwargs)

    def invoke_cmd(self, cmd):
        method_handle = getattr(self.data_store, str(cmd), None)

        if method_handle:
            out = method_handle(cmd)

        return out

    def start(self, threaded=True):
        if threaded:
            thread = Thread(target=self.serve_forever)
            thread.setDaemon(False)
            thread.start()

            return thread
        else:
            self.serve_forever()
