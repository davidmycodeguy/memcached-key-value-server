from key_val_svr.components.console.exceptions import InvalidCommandException
from key_val_svr.settings import DEFAULT_TIMEOUT


class BaseCommand(object):

    _name = None

    def validate(self, inputs):
        raise NotImplementedError()

    def get_msg(self, output):
        raise NotImplementedError()

    def __str__(self):
        return self._name


class GetCommand(BaseCommand):
    _name = "get"

    def __init__(self, inputs):
        self.key = None
        self.validate(inputs)

    def validate(self, inputs):
        if len(inputs) != 1:
            return InvalidCommandException(inputs)

        self.key = inputs[0]

    def get_msg(self, output):
        if not output:
            return "{} not present".format(self.key)

        return "{}: {}".format(self.key, output)


class SetCommand(BaseCommand):
    _name = "set"

    def __init__(self, inputs):
        self.key = None
        self.val = None
        self.ttl = DEFAULT_TIMEOUT

        self.validate(inputs)

    def validate(self, inputs):
        if len(inputs) < 2:
            raise InvalidCommandException(inputs)

        if len(inputs) >= 2:
            if len(inputs) == 2:
                self.key, self.val = inputs

            else:
                self.key, self.val, self.ttl = inputs[:3]
        else:
            raise InvalidCommandException(inputs)

    def get_msg(self, output):
        return "Success."


class DeleteCommand(BaseCommand):
    _name = "delete"

    def __init__(self, inputs):
        self.key = None
        self.validate(inputs)

    def validate(self, inputs):
        if len(inputs) != 1:
            raise InvalidCommandException(inputs)

        self.key = inputs[0]

    def get_msg(self, output):
        return True if output else False
