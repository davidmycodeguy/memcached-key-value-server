import os
import json
import socketserver
from http.server import SimpleHTTPRequestHandler
from threading import Thread
from urllib.parse import urljoin


class KeyValueWebServer(socketserver.TCPServer):

    def __init__(self, *args, **kwargs):
        self.data_store = kwargs.pop("data_store")

        super(KeyValueWebServer, self).__init__(*args, **kwargs)
        self.templates_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "templates")

    def start(self, threaded=True):
        if threaded:
            thread = Thread(target=self.serve_forever)
            thread.setDaemon(False)
            thread.start()

            return thread
        else:
            self.serve_forever()


class KeyValueHttpRequestHandler(SimpleHTTPRequestHandler):

    def get_key_val_server_stats(self):
        return self.server.data_store.get_stats()

    def do_GET(self):
        if self.path.find("/api") == 0:
            return self.wfile.write(bytes(json.dumps(self.get_key_val_server_stats()), encoding='utf-8'))

        return super(KeyValueHttpRequestHandler, self).do_GET()

    def translate_path(self, path):
        if path == "/":
            path = "/index.html"

        path = urljoin(self.server.templates_dir, path)

        return super(KeyValueHttpRequestHandler, self).translate_path(path)

